#!/bin/bash

# os=`cat /etc/os-release | grep -w ID | tr '"' ' ' | cut -d"=" -f2`
# echo "$os"

# if [ $os == ubuntu ]
# then
# 	echo "OS is ubuntu"
# 	sudo apt update
# elif [ $os == rhel ]
# then
# 	echo "OS is rhel"
#         sudo yum update -y	
# else
# 	echo "unknown OS"
# fi

#
 
group_name=$(awk -F ':' '{ if ( $1 == "group_name" ) { print $2 } }' properties.txt)
echo $group_name

user_name=$(awk -F ':' '{ if ( $1 == "user_name" ) { print $2 } }' properties.txt)
echo $user_name

sudo groupadd $group_name

sudo useradd -g $group_name $user_name

major_version=$(awk -F ':' '{ if ( $1 == "major_version" ) { print $2 } }' properties.txt)
echo $major_version
minor_version=$(awk -F ':' '{ if ( $1 == "minor_version" ) { print $2 } }' properties.txt)
echo $minor_version

wget https://apachemirror.wuchna.com/kafka/"$major_version"/kafka_"$minor_version".tgz 

# sudo chown $user_name:$group_name kafka_"$minor_version".tgz

sudo tar -xf kafka_"$minor_version".tgz -C /opt/

sudo chown -R $user_name:$group_name /opt/kafka_"$minor_version"
